const cors = require('cors');
import {Application, Router, urlencoded,json} from 'express';
import { createConnection } from 'typeorm';
import { SendResponse } from './model/response';
require('dotenv').config();

const app: Application = require('express')();

let profileRoute:Router = require('./router/profile');

console.log('Connecting to database');
createConnection().then(()=>{
    console.log('Database connected');
}).catch(err=> console.log(`Database connection error : ${err}`));

app.use(cors());
app.use(urlencoded({extended:true}));
app.use(json())

app.get('/',(req,res)=>{
    return res.send(process.env.SERVICE_NAME)
});

app.use('/v1',profileRoute);

app.use((req, res) => SendResponse.notFound(res, "Endpoint not found!"));

app.listen(process.env.PORT, ()=> console.log(`${process.env.SERVICE_NAME} running on port ${process.env.PORT}`));