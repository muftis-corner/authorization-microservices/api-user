import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Address } from "../entity/Address";
import { Email } from "../entity/Email";
import { Person } from "../entity/Person";
import { Phone } from "../entity/Phone";
import { Social } from "../entity/Social";
import { SendResponse } from "../model/response";
import { DataUtil } from "../utils/data";
import { HeaderUtils } from "../utils/header";

export class ProfileController {
    static async createProfile(req:Request, res:Response){

        let person:Person = new Person();
        let email: Email = new Email();
        // let phone:Phone = new Phone();

        let payload =  HeaderUtils.TokenPayload(req.headers.authorization); 
        let uuid = payload.id; 

        person.user_uuid = uuid;
        person.firstName = req.body.firstName;
        person.middleName = req.body.middleName;
        person.lastName = req.body.lastName;
        person.birthdate = new Date(req.body.birthdate);
        person.gender =req.body.gender;

        // email.email= payload.email;
        // email.isPrimary =true;
        // email.user_uuid = uuid;
        
        try {
            let personRepo = getRepository(Person);
            // let emailRepo = getRepository(Email);
            // let phoneRepo = getRepository(Phone);

            await personRepo.save(person).then(()=>console.log('person-complete'));            
            // await emailRepo.save(email).then(()=>console.log('email-complete'));            
            // await phoneRepo.save(phone).then(()=>console.log('phone-complete'));            
            
            return SendResponse.created(res,{id:uuid.id},`new profile created`)

        } catch (error) {
            console.log(error);
            return SendResponse.internalServerError(res,error);
        }
    }

    static async readProfile(req:Request,res:Response){
        
        let payload =  HeaderUtils.TokenPayload(req.headers.authorization); 
        let uuid = payload.id; 
        
        try {
            let personRepo = getRepository(Person);
            let emailRepo = getRepository(Email);
            let phoneRepo = getRepository(Phone);
            let socialRepo = getRepository(Social);
            let addressRepo = getRepository(Address);

            let person:Person = await personRepo.findOne({user_uuid:uuid});
            let email:Email[] = await emailRepo.find({user_uuid:uuid});
            let phone:Phone = await phoneRepo.findOne({user_uuid:uuid});
            let social:Social[]= await socialRepo.find({user_uuid:uuid});
            let address:Address = await addressRepo.findOne({user_uuid:uuid});


            if(!person){
                return SendResponse.notFound(res,'user not found');
            }

            if(email){
                console.log(email);
                email = email.map(item=> DataUtil.clean(item));
            }
            
            if(social){
                social = social.map(item=> DataUtil.clean(item));
            }
            

            let data = {
                uuid:uuid,
                person:DataUtil.clean(person),
                email:email,
                phone:DataUtil.clean(phone),
                social:social,
                address:DataUtil.clean(address)
            }
            
            return SendResponse.ok(res, data, 'user data found');

        } catch (error) {
            console.log(error);
            return SendResponse.internalServerError(res,error);   
        }
    }
}