import axios from "axios";
import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Email } from "../entity/Email";
import { SendResponse } from "../model/response";
import { DataUtil } from "../utils/data";
import { HeaderUtils } from "../utils/header";
export default class EmailController {

    static async create(req:Request,res:Response){
        console.log('create email called')
        let email:Email = new Email();
        let uuid = HeaderUtils.uuid(req.headers.authorization);

        email.email = req.body.email;;
        email.user_uuid = uuid;
        if(req.body.isPrimary){
           email.isPrimary = req.body.isPrimary;
        } else {
            email.isPrimary = false;
        }

        try {
            let emailRepo = getRepository(Email);
            await emailRepo.save(email);
            return SendResponse.created(res,{uuid:uuid,email:email},'new Email added')
        } catch (error) {
            console.log(error);
            return SendResponse.internalServerError(res,error);
        }
    }    

    static async check(req:Request, res:Response){
        let emailRepo = getRepository(Email);

        try {
            let email:Email = await emailRepo.findOne({email: req.body.email});
            if (email){
                return SendResponse.ok(res, "email found!");
            } 
            return SendResponse.notFound(res,"email not found !");
        } catch (error) {
            return SendResponse.internalServerError(res,error);
        }
    }

    static async read(req:Request,res:Response){
        let uuid = HeaderUtils.uuid(req.headers.authorization);

        try{
            let emailRepo = getRepository(Email);
            
            let emails:Email[] = await emailRepo.find({user_uuid:uuid});
            console.log(emails);
            if(emails.length <1) return SendResponse.notFound(res, "email not found")
            emails = emails.map(item=> DataUtil.clean(item,{saveId:true}));
            return SendResponse.ok(res,emails,"email fetched");
        } catch (error){
            return SendResponse.internalServerError(res,error);
        }
    }

    static async update(req:Request,res:Response){
        let uuid = HeaderUtils.uuid(req.headers.authorization);

        try {
            let emailRepo = getRepository(Email);
            let email = await emailRepo.findOne({email:req.body.oldEmail, user_uuid:uuid})

            if(!email){
                return SendResponse.notFound(res, 'email not found');
            }
            if(!req.body.newEmail){
                return SendResponse.badRequest(res, 'new email must not empty!');
            }
            email.email = req.body.newEmail;
            
            await emailRepo.save(email);
            return SendResponse.ok(res,email, 'email successfully changed!');

        } catch (error){
            return SendResponse.internalServerError(res,error)
        }
    }

    static async delete(req:Request,res:Response){
        let uuid = HeaderUtils.uuid(req.headers.authorization);

        try {
            let emailRepo = getRepository(Email);
            let email = await emailRepo.findOne({email:req.body.email, user_uuid:uuid})

            if(!email){
                return SendResponse.notFound(res, 'email not found');
            }

            await emailRepo.delete(email);
            return SendResponse.ok(res,email.email, 'email successfully removed!');

        } catch (error){
            console.log(error);
            return SendResponse.internalServerError(res,error)
        }
    }

    static async changePrimary(req:Request,res:Response){
        let uuid = HeaderUtils.uuid(req.headers.authorization);

        try {
            let emailRepo = getRepository(Email);
            let oldPrimaryEmail = await emailRepo.findOne({isPrimary:true, user_uuid:uuid});
            let newPrimaryEmail = await emailRepo.findOne({id:req.body.idEmail, isPrimary:false, user_uuid:uuid});

            if(oldPrimaryEmail.id == req.body.idEmail){
                return SendResponse.badRequest(res, "nothing need to change !");
            }

            oldPrimaryEmail.isPrimary = false;
            newPrimaryEmail.isPrimary = true;

            await emailRepo.save(oldPrimaryEmail);
            await emailRepo.save(newPrimaryEmail);

            let auth_url = process.env.CREDENTIAL_API+'/updateEmail'; 
            let token = HeaderUtils.TokenSlicer(req.headers.authorization);
            let header ={"Authorization": `Bearer ${token}`};

            await axios({
                method:"post",
                url:auth_url,
                headers: header,
                data:{
                    email:newPrimaryEmail.id
                }
            })
            .catch(err=> SendResponse.internalServerError(res,err));
            
            return SendResponse.ok(res,{email:newPrimaryEmail.email,isPrimary:newPrimaryEmail.isPrimary},'primary email changed');
        
        } catch (error) {
            console.log(error)
            return SendResponse.internalServerError(res,error)
        }
    }

    static async getPrimary(req:Request,res:Response){

        let uuid = HeaderUtils.uuid(req.headers.authorization);

        try {
            let emailRepo = getRepository(Email);
            let primaryEmail = await emailRepo.findOne({isPrimary:true, user_uuid:uuid});
            primaryEmail = DataUtil.clean(primaryEmail,{saveId:true});
                return SendResponse.ok(res,primaryEmail,'primary email changed');       
        } catch (error) {
            console.log(error)
            return SendResponse.internalServerError(res,error)
        }
    }
    
}