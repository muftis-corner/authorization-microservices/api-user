export class DataUtil {
    static clean(data,options?:{saveId?:boolean,},){
        if(data){
            if(!options.saveId){
                delete data.id;
            }
        delete data.user_uuid;
        delete data.createdAt;
        delete data.updateAt;
        return data;
        }
    }
}