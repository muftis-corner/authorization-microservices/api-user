import { verify } from "jsonwebtoken";

export class HeaderUtils{
    static TokenSlicer(bearer) {
        if (bearer.startsWith("Bearer ")) {
          return bearer.slice(7, bearer.length);
        } return false;
      }

    static TokenPayload(bearer){
      let token = this.TokenSlicer(bearer);
      let decode = verify(token,process.env.SECRET);
      return decode['data'];
    }
    static uuid(bearer){
      let data = this.TokenPayload(bearer);
      
      return data.id;
    }
}

 
  