import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";

@Entity()
export class Phone {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_uuid: string;

    @Column()
    phone:string;

    @Column()
    country:string;

    @CreateDateColumn({type:"timestamp" ,default: ()=>"CURRENT_TIMESTAMP(6)"})
    createdAt: Date;
    
    @UpdateDateColumn({type:"timestamp" ,default: ()=>"CURRENT_TIMESTAMP(6)",onUpdate:"CURRENT_TIMESTAMP(6)"})
    updateAt: Date;
}
