import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";

@Entity()
export class Address {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_uuid: string;

    @Column()
    country: string;

    @Column()
    state:string;

    @Column()
    city:string;

    @Column()
    district:string;

    @Column()
    street:string;

    @Column()
    apt_suite:string;

    @CreateDateColumn({type:"timestamp" ,default: ()=>"CURRENT_TIMESTAMP(6)"})
    createdAt: Date;
    
    @UpdateDateColumn({type:"timestamp" ,default: ()=>"CURRENT_TIMESTAMP(6)",onUpdate:"CURRENT_TIMESTAMP(6)"})
    updateAt: Date;
}
