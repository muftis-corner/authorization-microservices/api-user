import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn} from "typeorm";

@Entity()
export class Person {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({unique:true})
    user_uuid: string;

    @Column()
    firstName: string;

    @Column()
    middleName: string;
    
    @Column()
    lastName: string;

    @Column()
    birthdate: Date;

    @Column()
    gender:number;

    
    @Column({nullable:true})
    avatar:string;

    @CreateDateColumn({type:"timestamp" ,default: ()=>"CURRENT_TIMESTAMP(6)"})
    createdAt: Date;
    
    @UpdateDateColumn({type:"timestamp" ,default: ()=>"CURRENT_TIMESTAMP(6)",onUpdate:"CURRENT_TIMESTAMP(6)"})
    updateAt: Date;

}
