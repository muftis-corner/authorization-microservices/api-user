import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";

@Entity()
export class Email {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_uuid: string;

    @Column({unique:true})
    email:string;

    @Column({default:false})
    isPrimary:boolean;

    @CreateDateColumn({type:"timestamp" ,default: ()=>"CURRENT_TIMESTAMP(6)"})
    createdAt: Date;
    
    @UpdateDateColumn({type:"timestamp" ,default: ()=>"CURRENT_TIMESTAMP(6)",onUpdate:"CURRENT_TIMESTAMP(6)"})
    updateAt: Date;
}
