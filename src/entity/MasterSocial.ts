import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class MasterSocial {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    url: string;
}
