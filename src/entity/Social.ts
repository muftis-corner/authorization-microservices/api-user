import {Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";
import { MasterSocial } from "./MasterSocial";

@Entity()
export class Social {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_uuid: string;

    @Column()
    username:string;

    @OneToOne(type => MasterSocial) @JoinColumn() 
    socialPlatform: MasterSocial;

    @CreateDateColumn({type:"timestamp" ,default: ()=>"CURRENT_TIMESTAMP(6)"})
    createdAt: Date;
    
    @UpdateDateColumn({type:"timestamp" ,default: ()=>"CURRENT_TIMESTAMP(6)",onUpdate:"CURRENT_TIMESTAMP(6)"})
    updateAt: Date;

    
}
