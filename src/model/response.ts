import { Response } from "express";

let prod = require("../config/prod");

export class SendResponse {
  static ok = (res: Response, data?: any, message?: string) =>
    res.status(200).json({
      status: true,
      message: message,
      data: data,
    });
  static created = (res: Response, data?: any, message?: string) =>
    res.status(201).json({
      status: true,
      message: message,
      data: data,
    });
  static accepted = (res, message) =>
    res.status(202).json({
      status: true,
      message: message,
    });
  static noData = (res, message) =>
    res.status(204).json({
      status: true,
      message: message,
    });
  static badRequest = (res, message?:string) =>
    res.status(400).json({
      status: false,
      message: message ?? "Bad Request"
    });
  static unauthorized = (res, message?) =>
    res.status(401).json({
      status: false,
      message: message ?? "Unauthorized"
    });
  static forbidden = (res, message?) =>
    res.status(403).json({
      status: false,
      message: message ?? "Forbidden"
    });
  static notFound = (res, message?) =>
    res.status(404).json({
      status: false,
      message: message ?? "Not Found"
    });
  static requestTimedOut = (res, message?) =>
    res.status(408).json({
    status: false,
    message: message ?? "Request Timed Out"
    });
  static internalServerError = (res, error) => {
    if (prod()) {
      res.status(500).json({
        status: false,
        message: "Oops Something Went Wrong",
        data: "Internal Server Error"
      });
    } else {
      res.status(500).json({
        status: false,
        message: "Oops Something Went Wrog",
        data: error
      });
    }
  };
}
