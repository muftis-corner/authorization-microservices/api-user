import { Router } from "express";
import EmailController from "../controller/emailController";
import { ProfileController } from "../controller/profileController";
import { Email } from "../entity/Email";
import { JwtMiddleware } from "../middleware/jwt";


let router:Router = Router();

router
    .route('/check')
    .post(EmailController.check)

router
    .use(JwtMiddleware.checkToken)
    .route('/profile')
    .post(ProfileController.createProfile)
    .get(ProfileController.readProfile);

router
    .use(JwtMiddleware.checkToken)
    .route('/email')
    .post(EmailController.create)
    .get(EmailController.read)
    .put(EmailController.update)
    .delete(EmailController.delete);

router
    .use(JwtMiddleware.checkToken)
    .route('/email/primary')
    .post(EmailController.changePrimary)
    .get(EmailController.getPrimary);

module.exports =router;